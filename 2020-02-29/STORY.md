# ストーリー #

- LuaLaTeXを用いる
  - LuaTeX-jaを用いる
    - https://ja.osdn.net/projects/luatex-ja/
    - https://ja.osdn.net/projects/luatex-ja/wiki/LuaTeX-ja%E3%81%AE%E4%BD%BF%E3%81%84%E6%96%B9
    - 今回は日本語をメインで用いるための話をする

- 仮定する環境
  - TeX Live 2019（frozen）
    - 2020-02-28にfrozenとなる／なった予定
    - https://tug.org/texlive/

- 基本的なLuaTeX-jaを用いたPDFの作成方法
  - documentclass
  - タイプセットコマンド
  - プリアンブル

- LuaTeX-jaが提供する書体の変更方法
  - パッケージ
    - luatexja-fontspec

- フォントを置く場所
  - kpsewhich -var-value=VARTEXFONTS
    - $VARTEXFONTS/opentypeの下にOTFファイルを配置する
    - TTFなら$VARTEXFONTS/truetypeの下
    - ディレクトリ名はtexmf-dist/fontsを参照
  - OSの標準配置場所で読めることもある

- 和文書体を変更する
  - 原ノ味明朝
    - https://github.com/trueroad/HaranoAjiFonts
    - https://github.com/trueroad/tr-TeXConf2019
  - \setmainjfont
  - \setsansjfont

- 指定するファイル名を調べる
  - otfinf -a
  - ファイル名でもOK

- 欧文書体を変更する
  - \setmainfont
  - \setsansfont

- Math Fonts in CTAN
  - https://ctan.org/topic/font-maths?lang=en

- UD書体の話
  - MORISAWA BIZ+
    https://www.morisawa.co.jp/products/fonts/bizplus/
    - macOSなら/Library/Fonts/以下にインストールされる
      - BIZ-UDGothicB.ttc
      - BIZ-UDMinchoM.ttc
      - BIZ-UDGothicR.ttc
  - UD Mathはまだ現実的ではない
    - 欧文のUD
      - モリサワ
    - 従属欧文を使っての数式組版もやろうと思えばできるが非現実的
      - 記号の不足
      - 小学校程度なら可能かも
  - なんでもかんでもUDであればよいというわけではない
    - 字形がUDに振られているためこれまでの書体とはデザインが異なる
    - 健常者にとっては違和感がでる場合がある
    - いわゆる優美な書体とはコンセプトがちがう

- 数式を組む書体
  - https://twitter.com/zr_tex8r/status/1040047027005079552
  - Math Font Selection in LaTeX and Unicode
    - http://milde.users.sourceforge.net/LUCR/Math/math-font-selection.xhtml
    - https://www.latex-project.org/help/documentation/fntguide.pdf
  - unicode-math
  - http://www.unicode.org/reports/tr25/tr25-15.pdf

- 数式を組む用途に適した書体か否かを知るには
  - OTF
    - MATH table
  - ttx
    - fonttools
    - https://github.com/fonttools/fonttools
